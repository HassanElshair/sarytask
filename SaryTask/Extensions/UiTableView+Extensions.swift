//
//  UiTableView+Extensions.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import Foundation
import UIKit

extension UITableView {
    
    func registerCellNib<Cell: UITableViewCell>(cellClass: Cell.Type){
        self.register(UINib(nibName: String(describing: Cell.self), bundle: nil),
                       forCellReuseIdentifier: String(describing: Cell.self))
    }
    
    func dequeue<Cell: UITableViewCell>() -> Cell{
        let identifier = String(describing: Cell.self)
        
        guard let cell = self.dequeueReusableCell(withIdentifier: identifier) as? Cell else {
            // this line for me as adevloper no a user to help me to know where is a problem becuse it block my code in this line to help me to detecte where is a problem
            fatalError("Error in cell")
        }
        
        return cell
    }
    
}
