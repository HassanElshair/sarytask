//
//  UiViewController+Extensions.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import UIKit

private let spinnerTag = 101010101014510
extension UIViewController {
    
    func setupNavigation(){
        let logoImage = UIImage.init(named: "social-1")
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame =  CGRect(x: -300, y: -100, width: 0, height: 0)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -300
        navigationItem.leftBarButtonItems = [negativeSpacer, imageItem]
    }
    
    
    func showSpinner(onView: UIView, backColor: UIColor = UIColor.black.withAlphaComponent(0)) {
        let spinnerView = UIView(frame: onView.bounds)
        spinnerView.backgroundColor = backColor
        //
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        blurEffectView.addCornerRadius(10)
        blurEffectView.clipsToBounds = true
        blurEffectView.center = spinnerView.center
        spinnerView.addSubview(blurEffectView)
        //
        var ai = UIActivityIndicatorView()
        if #available(iOS 13, *) {
            ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        } else {
            ai = UIActivityIndicatorView(style: .whiteLarge)
        }
        ai.color = .white
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        spinnerView.tag = spinnerTag
    }
    
    func removeSpinner(fromView: UIView) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            var loader: UIView? = fromView.viewWithTag(spinnerTag)
            UIView.animate(withDuration: 0.2, animations: {
                loader?.alpha = 0
            }, completion: { _ in
                loader?.removeFromSuperview()
                loader = nil
            })
        }
    }
    
}
