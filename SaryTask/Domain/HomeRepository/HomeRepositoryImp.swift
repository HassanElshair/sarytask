//
//  HomeRepositoryImp.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol HomeRepository {
    func fetchCatalogData() -> Observable<[CatalogResult]>
}

class HomeRepositoryImp:HomeRepository {
    
    let networkClient :NetworkClient
    static let shared = HomeRepositoryImp()
    
    init(networkClient:NetworkClient = NetworkClient()) {
        self.networkClient = networkClient
    }
    
    func fetchCatalogData() -> Observable<[CatalogResult]>{
        Observable<[CatalogResult]>.create { (item) -> Disposable in
            NetworkClient().performRequest(ResponseObject<[CatalogResult]>.self, router: MainRouter.getCatalogData) { result  in
                switch result {
                case .success(let data):
                    item.onNext(data.result)
                    item.onCompleted()
                case .failure(let error):
                    item.onError(error)
                }
            }
            return Disposables.create()
        }
    }
}
