//
//  ApiRouter.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import Foundation
import Alamofire

protocol APIRouter:URLRequestConvertible {
    var method:HTTPMethod {get}
    var path: String {get}
    var parameters:Parameters? {get}
    var encoding:ParameterEncoding {get}
}

extension APIRouter{
    
    func asURLRequest() throws -> URLRequest {
        guard var url = URL(string: Constants.baseUrl) else{
            throw appError.urlNotValid
        }
        url.appendPathComponent(path)
        let request =  try URLRequest(url: url, method: method, headers: nil)
        return try encoding.encode(request, with: parameters)
    }
}
