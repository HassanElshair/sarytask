//
//  NetworkClient.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import Foundation
import Alamofire
import Reachability

class NetworkClient{
    
    fileprivate var reachability: Reachability?
    
    func isInternetAvailable() -> Bool {
        return (NetworkReachabilityManager()?.isReachable)!
    }
    
    func performRequest<T:Decodable>(_ object:T.Type, router:APIRouter ,completion: @escaping ((Result<T,Error>) -> Void)) {
        if isInternetAvailable() {
        AF.request(router).responseJSON { (response) in
            do {
                let models = try JSONDecoder().decode(T.self, from: response.data ?? Data())
                completion(.success(models))
            } catch let error {
                completion(.failure(error))
            }
        }
        }else{
            completion(.failure(BaseError.NoInternet))
        }
    }
}
