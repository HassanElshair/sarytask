//
//  MainRouter.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import Foundation
import Alamofire

enum MainRouter: APIRouter{
   
    case getCatalogData
    
    var method:HTTPMethod {
        switch self{
        case .getCatalogData:
           return .get
        }
    }
    
    var path: String{
        switch self{
        case .getCatalogData:
            return "catalog"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getCatalogData:
           return nil
        }
    }

    
    var encoding:ParameterEncoding{
        switch self {
        case .getCatalogData:
            return URLEncoding.default
        }
    }
}


enum appError:Error{
    case urlNotValid
}
