//
//  LocalizedButton.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import UIKit
import MOLH

class LocalizedButton: UIButton{
    
    override func awakeFromNib() {
        self.setTitle(self.currentTitle?.localized, for: .normal)
    }
}
