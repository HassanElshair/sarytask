//
//  BaseError.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import Foundation

enum BaseError: Error {
    case NoInternet
}

extension BaseError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .NoInternet:
            return "NoInternet"
        }
    }
}
