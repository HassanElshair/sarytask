//
//  LocalizedLabel.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import UIKit
import MOLH

class LocalizedLabel: UILabel{
    
    override func awakeFromNib() {
        if let txt = self.text{
            self.text = txt.localized
        }
        adjustAutoAlignment()
    }
    
    //MARK:-Alignment
    func adjustAutoAlignment(){
        if self.textAlignment != .center {
            if MOLHLanguage.isArabic() {
                self.textAlignment = .right
            }else{
                self.textAlignment = .left
            }
        }
    }
}
