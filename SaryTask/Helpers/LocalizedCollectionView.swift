//
//  LocalizedCollectionView.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import UIKit
import MOLH

class LocalizedCollectionView: UICollectionViewFlowLayout {
    override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return MOLHLanguage.isArabic()
    }
}
