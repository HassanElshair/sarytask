//
//  AppDelegate.swift
//  SaryTask
//
//  Created by HassanElshair on 02/09/2021.
//

import UIKit
import MOLH

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.rootViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        MOLH.shared.activate(true)
        if MOLHLanguage.isRTLLanguage() {
            UICollectionView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        window?.makeKeyAndVisible()
        
        return true
    }
}
