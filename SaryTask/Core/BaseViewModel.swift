//
//  BaseViewModel.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import Foundation
import RxCocoa
import RxSwift

class baseViewModel:viewModel{
    var isLoading:PublishSubject<Bool> = .init()
    var displayErorr:PublishSubject<String> = .init()
}
