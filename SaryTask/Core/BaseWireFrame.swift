//
//  BaseWireFrame.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import UIKit
import RxCocoa
import RxSwift

protocol viewModel {
    
}

class BaseWireFrame<T:baseViewModel>:UIViewController {
    
    //MARK:variables
    var disposeBag = DisposeBag()
    var viewModel: T!
    
    //MARK:- Configure
    func configure(with viewModel: T) {
        fatalError("You did not override configure method.. ")
    }
    
    
    //MARK:LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        if viewModel == nil, let _ = self as? HomeViewController{
//            viewModel = HomeViewModel(homeRepository: HomeRepositoryImpl.shared) as? T
//        }
        bindLoadings()
        configure(with: viewModel)
    }
    
    //MARK:- BindStates
    func bindLoadings(){
       
        viewModel.displayErorr.subscribe(onNext: {[weak self ] text in
            self?.displayError(text:text )
        }).disposed(by: disposeBag)
        
        viewModel.isLoading.subscribe(onNext: {[weak self ] isLoading in
            if isLoading {
                DispatchQueue.main.async {
                    self?.view.isUserInteractionEnabled = false
                    self?.showSpinner(onView: (self?.view)!)
                }
            }else{
                DispatchQueue.main.async {
                    self?.view.isUserInteractionEnabled = true
                    self?.removeSpinner(fromView: (self?.view)!)
                }
            }
        }).disposed(by: disposeBag)
        
        //Reachability state binding
        
        
        
    }
}

extension BaseWireFrame{
    func displayError(text:String){
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        let cancelButton = UIAlertAction(title: "Ok", style: .destructive, handler: nil)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
}
