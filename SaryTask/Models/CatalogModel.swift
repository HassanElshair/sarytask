//
//  CatalogModel.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//


import Foundation

// MARK: - Welcome
struct CatalogModel: Codable {
    var result: [CatalogResult]?
    var other: Other?
    var message: String?
    var status: Bool?
}

// MARK: - Other
struct Other: Codable {
    var showSpecialOrderView: Bool?
    var uncompletedProfileSettings: UncompletedProfileSettings?
    var businessStatus: BusinessStatus?

    enum CodingKeys: String, CodingKey {
        case showSpecialOrderView = "show_special_order_view"
        case uncompletedProfileSettings = "uncompleted_profile_settings"
        case businessStatus = "business_status"
    }
}

// MARK: - BusinessStatus
struct BusinessStatus: Codable {
    var id: Int?
    var title: String?
}

// MARK: - UncompletedProfileSettings
struct UncompletedProfileSettings: Codable {
    var showTag: Bool?
    var message: String?
    var image: String?
    var isCompletedProfile: Bool?

    enum CodingKeys: String, CodingKey {
        case showTag = "show_tag"
        case message, image
        case isCompletedProfile = "is_completed_profile"
    }
}

// MARK: - Result
struct CatalogResult: Codable {
    var id: Int?
    var title: String?
    var data: [Datum]?
    var dataType: String?
    var showTitle: Bool?
    var uiType: String?
    var rowCount: Int?

    enum CodingKeys: String, CodingKey {
        case id, title, data
        case dataType = "data_type"
        case showTitle = "show_title"
        case uiType = "ui_type"
        case rowCount = "row_count"
    }
}

// MARK: - Datum
struct Datum: Codable {
    var groupID: Int?
    var filters: [Filter]?
    var name: String?
    var image: String?
    var emptyContentImage: String?
    var emptyContentMessage: String?
    var hasData, showUnavailableItems, showInBrochureLink: Bool?
    var deepLink: String?

    enum CodingKeys: String, CodingKey {
        case groupID = "group_id"
        case filters, name, image
        case emptyContentImage = "empty_content_image"
        case emptyContentMessage = "empty_content_message"
        case hasData = "has_data"
        case showUnavailableItems = "show_unavailable_items"
        case showInBrochureLink = "show_in_brochure_link"
        case deepLink = "deep_link"
    }
}

// MARK: - Filter
struct Filter: Codable {
    var filterID: Int?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case filterID = "filter_id"
        case name
    }
}

