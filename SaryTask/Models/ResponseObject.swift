//
//  ResponseObject.swift
//  SaryTask
//
//  Created by HassanElshair on 03/09/2021.
//

import Foundation

struct ResponseObject<T:Decodable>:Decodable{
    var message:String
    var success:String
    var result:T
}
